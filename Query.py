from flask import Flask, jsonify, request
import sqlite3

app = Flask(__name__)

# Function to get the recent ten articles
def get_recent_articles():
    conn = sqlite3.connect('arxiv_cs_ai.db')
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM articles ORDER BY published DESC LIMIT 10")
    articles = cursor.fetchall()
    print(articles)  # Debugging line
    conn.close()
    return articles

# Function to get article data by ID
def get_article_by_id(article_id):
    conn = sqlite3.connect('arxiv_cs_ai.db')
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM articles WHERE id=?", (str(article_id),))
    article = cursor.fetchone()
    conn.close()
    return article

# Function to search articles by keyword
def search_articles_by_keyword(keyword):
    conn = sqlite3.connect('arxiv_cs_ai.db')
    cursor = conn.cursor()
    query = f"%{keyword}%"
    cursor.execute("SELECT * FROM articles WHERE title LIKE ?", (query,))
    articles = cursor.fetchall()
    conn.close()
    return articles

@app.route('/')
def index():
    recent_articles = get_recent_articles()
    recent_articles_dict = [{'id': a[0], 'title': a[1], 'summary': a[2], 'published': a[3], 'updated': a[4], 'link': a[5]} for a in recent_articles]
    return jsonify({
        'recent_documents': recent_articles_dict
    })

# Getting metadata for specific document by searching ID
@app.route('/id/<document_id>')
def get_document_by_id(document_id):
    article = get_article_by_id(document_id)
    if article:
        article_data = {'id': article[0], 'title': article[1], 'summary': article[2], 'published': article[3], 'updated': article[4], 'link': article[5]}
        return jsonify(article_data)
    else:
        return jsonify({'error': 'Document not found'}), 404

# Getting metadata for specific document by searching keywords which are included in titles
@app.route('/search/<keyword>')
def search_document_by_keyword(keyword):
    if not isinstance(keyword, str) or keyword.strip() == "":
        return jsonify({'error': 'Invalid or empty keyword format'}), 400

    articles = search_articles_by_keyword(keyword)
    if articles:
        articles_dict = [{'id': a[0], 'title': a[1], 'summary': a[2], 'published': a[3], 'updated': a[4], 'link': a[5]} for a in articles]
        return jsonify(articles_dict)
    else:
        return jsonify({'error': 'No articles found for the given keyword'}), 404


if __name__ == '__main__':
    app.run(debug=True)
