import sqlite3
import nltk
nltk.download('stopwords')
nltk.download('punkt')
nltk.download('wordnet')
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from collections import Counter

# connection db
conn = sqlite3.connect('arxiv_cs_ai.db')
cursor = conn.cursor()

cursor.execute("SELECT summary FROM articles")
abstracts = cursor.fetchall()

# Pretreatment NLTK
lemmatizer = WordNetLemmatizer()
stop_words = set(stopwords.words('english'))

# Analysis
for abstract in abstracts:
    # tokenize
    words = word_tokenize(abstract[0])
    # remove punctuations and spaces
    filtered_words = [word.lower() for word in words if word.isalpha() and word.lower() not in stop_words]
    # lemmatize
    lemmatized_words = [lemmatizer.lemmatize(word) for word in filtered_words]
    # counter
    word_counts = Counter(lemmatized_words)
    # results
    print(word_counts)

conn.close()
