import requests
import sqlite3
import xml.etree.ElementTree as ET
import re

# URL for the arXiv API
ARXIV_API_URL = "http://export.arxiv.org/api/query"


# Function to fetch data from arXiv API
def fetch_arxiv_data(query, max_results=100):
    params = {
        "search_query": query,
        "start": 0,
        "max_results": max_results
    }
    response = requests.get(ARXIV_API_URL, params=params)
    return response.text


# Function to parse the XML response from arXiv API
def parse_xml(xml_data):
    root = ET.fromstring(xml_data)
    namespace = {'arxiv': 'http://www.w3.org/2005/Atom'}
    entries = root.findall('arxiv:entry', namespace)

    parsed_data = []
    for entry in entries:
        full_id = entry.find('arxiv:id', namespace).text
        short_id = re.search(r'arxiv.org/abs/(.+)', full_id).group(1)
        link = entry.find('arxiv:link[@rel="alternate"]', namespace).attrib['href']

        data = {
            'id': short_id,
            'title': entry.find('arxiv:title', namespace).text,
            'summary': entry.find('arxiv:summary', namespace).text,
            'published': entry.find('arxiv:published', namespace).text,
            'updated': entry.find('arxiv:updated', namespace).text,
            'link': link
        }
        parsed_data.append(data)
    return parsed_data


# Function to create database and tables
def create_db():
    conn = sqlite3.connect('arxiv_cs_ai.db')
    cursor = conn.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS articles (
            id TEXT PRIMARY KEY,
            title TEXT,
            summary TEXT,
            published TEXT,
            updated TEXT,
            link TEXT
        )
    ''')
    conn.commit()
    conn.close()


# Function to insert data into the database
def insert_data_to_db(data):
    conn = sqlite3.connect('arxiv_cs_ai.db')
    cursor = conn.cursor()

    for article in data:
        cursor.execute('''
            INSERT OR IGNORE INTO articles (id, title, summary, published, updated, link) 
            VALUES (?, ?, ?, ?, ?, ?)
        ''', (article['id'], article['title'], article['summary'], article['published'], article['updated'], article['link']))

    conn.commit()
    conn.close()


# Main script execution
def main():
    # Create database and table
    create_db()

    # Fetch data from arXiv API
    xml_data = fetch_arxiv_data("cs.AI")

    # Parse the XML data
    parsed_data = parse_xml(xml_data)

    # Insert data into the database
    insert_data_to_db(parsed_data)

    print("Data fetched and stored in database.")


# Run the script
main()