from transformers import AutoTokenizer, AutoModelForSequenceClassification
import numpy as np
from pymongo import MongoClient

# Specify the directory containing the tokenizer files
tokenizer_dir = "../Filrouge/model"

# Load model and tokenizer from the specified directory
tokenizer = AutoTokenizer.from_pretrained(tokenizer_dir)
model = AutoModelForSequenceClassification.from_pretrained(tokenizer_dir)

def get_top_predictions(title, abstract):
    # Tokenize inputs
    inputs = tokenizer.encode_plus(f"<TITLE> {title}\n<ABSTRACT> {abstract}", return_tensors="pt", max_length=512, truncation=True)

    # Perform inference
    outputs = model(**inputs)

    # Get predicted logits and labels
    logits = outputs.logits.detach().cpu().numpy()[0]
    predicted_labels = np.argsort(logits)[::-1]  # Sort labels by probability in descending order

    # Decode all predicted labels along with their probabilities
    top_predictions = []
    for label_id in predicted_labels:
        label = model.config.id2label[label_id]  # Convert label ID to label name
        probability = logits[label_id]  # Get probability of the label
        top_predictions.append((label, probability))

    # Return the top 2 rated predictions
    return top_predictions[:2]

def process_collection(db):
    # Connect to the collection containing metadata
    metadata_collection = db["arxiv_metadata"]

    # Create a new collection for storing results
    results_collection = db["topic_predictions"]

    # Iterate through each document in the metadata collection
    for document in metadata_collection.find():
        # Extract title and summary from the document
        title = document.get("title", "")
        abstract = document.get("summary", "")

        # Get top predictions for the title and abstract
        predictions = get_top_predictions(title, abstract)

        # Store title, abstract, and predictions in the results collection
        results_collection.insert_one({
            "title": title,
            "abstract": abstract,
            "predictions": predictions
        })

if __name__ == "__main__":
    # Connect to MongoDB
    client = MongoClient("mongodb://localhost:27017/")
    db = client["arxiv"]

    # Process the collection
    process_collection(db)
