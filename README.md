# Project_Python_Fil_rouge

## Introduction

This project, the arXiv AI Papers Metadata Extractor, is designed to streamline the process of accessing and organizing scientific papers from the arXiv database, specifically in the field of Artificial Intelligence (AI). Utilizing the arXiv API, this script fetches papers labeled under the "cs.AI" category (Computer Science - Artificial Intelligence), extracts their metadata, and stores it in a local SQLite database for easy access and analysis. There are three scripts in this project: Query.py, Extraction.py, and DatabaseTest.py. Here're the purpose of each of them:

## Extraction.py

The first thing to run is Extraction.py, which utilizes ArXiv's API: "http://export.arxiv.org/api/query" to get the metadata that the user needs: such as title, synopsis, publication date, etc. The following briefly describes the types of metadata that are included in this API:

- `<title>`: The title of the article.
- `<id>`: A URL http://arxiv.org/abs/id
- `<published>`: The date that version 1 of the article was submitted.
- `<updated>`: The date that the retrieved version of the article was submitted. Same as `<published>` if the retrieved version is version 1.
- `<summary>`: The article abstract.

By running this script, a local database (arxiv_cs_ai.db) will be created via python's built-in sqlite3.

## Query.py

Contains a flask with three APIs:

- `app.route('/')`: The API responsible for the home page, with hints on how to enter a search and metadata on the ten most recent documents in the database.
- `app.route('/search/<keyword>')`: Returns the metadata of the document in question by searching for keywords contained in the document's title.
- `app.route('/id/<document_id>')`: Returns the metadata of the document by searching for the id of a specific document.

## DatabaseTest.py

Returns all data currently in the database.

## Unittest.py

unittest for two APIs to test whether the results passed back meet expectations by using mocked data

## Dependancies

Python version 3.9.13 with python libraries:

- unittest: Python's built-in testing framework for writing and running unit tests.

- sqlite3: A Python library for interacting with SQLite databases, providing a lightweight and serverless database solution.

- xml.etree.ElementTree: A module for parsing and manipulating XML data using a simple and efficient ElementTree API.

- requests: An HTTP library that simplifies making HTTP requests in Python, allowing you to interact with web services and APIs.

- re: The "re" module for regular expressions in Python, used to match and manipulate text patterns with powerful string matching capabilities.

- flask: A micro web framework for Python that simplifies web application development by providing tools and libraries for building web-based applications and APIs.
