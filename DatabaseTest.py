import sqlite3


def fetch_first_10_metadata(db_path):
    # Connect to the SQLite database
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()

    # Query to select the first 10 metadata entries
    query = "SELECT id, title, summary, published, updated FROM articles ORDER BY published DESC LIMIT 10"

    try:
        cursor.execute(query)
        records = cursor.fetchall()

        # Print the fetched records
        for record in records:
            print(
                f"ID: {record[0]}, Title: {record[1]}, Summary: {record[2]}, Published: {record[3]}, Updated: {record[4]}")
            print("-" * 50)  # Separator for readability

    except sqlite3.Error as e:
        print(f"Database error: {e}")
    finally:
        conn.close()


# Path to your SQLite database
db_path = 'arxiv_cs_ai.db'

# Fetch and print the first 10 metadata entries
fetch_first_10_metadata(db_path)
