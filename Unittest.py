import unittest
from Query import app
from unittest.mock import patch


class TestAPI(unittest.TestCase):
    def setUp(self):
        app.testing = True
        self.client = app.test_client()

    @patch('Query.get_article_by_id')
    def test_get_document_by_id(self, mock_get_article):
        # Mocking the get_article_by_id function
        mock_get_article.return_value = ['123', 'Test Title', 'Test Summary', '2021-01-01', '2021-01-02']

        # Simulating a request with an existing ID
        response = self.client.get('/id/123')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Test Title', response.json['title'])

        # Simulating a request with a non-existing ID
        mock_get_article.return_value = None
        response = self.client.get('/id/999')
        self.assertEqual(response.status_code, 404)
        self.assertIn('Document not found', response.json['error'])

        # Simulating a request with an invalid ID format
        response = self.client.get('/id/invalid_id')
        self.assertEqual(response.status_code, 400)
        self.assertIn('Invalid document ID format', response.json['error'])

    @patch('Query.search_articles_by_keyword')
    def test_search_document_by_keyword(self, mock_search):
        # Mocking the database response for a specific keyword
        mock_search.return_value = [
            ('2211.02992v1', 'Test Title 1', 'Test Summary 1', '2021-01-01', '2021-01-02', 'http://testlink1.com'),
            ('2211.02993v1', 'Test Title 2', 'Test Summary 2', '2021-01-03', '2021-01-04', 'http://testlink2.com')
        ]

        # Test with valid keyword
        response = self.client.get('/search/Test')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Test Title 1', response.json[0]['title'])
        self.assertIn('Test Title 2', response.json[1]['title'])

        # Test with keyword that yields no results
        mock_search.return_value = []
        response = self.client.get('/search/NonExistingKeyword')
        self.assertEqual(response.status_code, 404)
        self.assertIn('No articles found for the given keyword', response.json['error'])

        # Test with invalid keyword format (e.g., empty string)
        response = self.client.get('/search/ ')
        self.assertEqual(response.status_code, 400)
        self.assertIn('Invalid or empty keyword format', response.json['error'])


if __name__ == '__main__':
    unittest.main()
